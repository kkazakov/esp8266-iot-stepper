#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include <String.h>


#include "Settings.h"
#include "WebServer.h"
#include "Wifi.h"


#define STEPS 200  // Max steps for one revolution
#define RPM 60     // Max RPM

void sleep(unsigned long ms);
void deepsleep(unsigned long micros);

#endif
