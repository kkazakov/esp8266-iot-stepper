#include "Sensor.h"
#include "A4988.h"

boolean shouldStartWebServer = true;

long lastConnectTry = 0;

int status = WL_IDLE_STATUS;

boolean connect;

int CLR_BTN = 6;

#define MICROSTEPS 1

#define DIR 4 // GPIO16, D0
#define STEP 5 //GPIO14, D5
#define SLEEP 6 // GPIO12, D7

#define M0 7 // GPIO13, D7
#define M1 16 // GPIO15, D8

#define STOP_SENSOR 17 // GPIO2, D4

A4988 stepper(STEPS, DIR, STEP, SLEEP, M0, M1);


void setup () {
  delay(1000);
  Serial.begin(9600);

  Serial.println();
  Serial.println(FW_VERSION);

  SPIFFS.begin();

  delay(100);

  Serial.println("initializing stepper ...");
  stepper.begin(RPM, MICROSTEPS);


  Serial.println("rotating 360 degrees ...");
  // TEST rotate
  stepper.rotate(360);

  // GO BACK using microsteps
  Serial.println("moving back with microsteps ...");
  stepper.move( - STEPS * MICROSTEPS);

  if (digitalRead(CLR_BTN) == LOW) {
    Serial.println("CLR PRESSED!!!");

    delay(10000);
    
    if (digitalRead(CLR_BTN) == LOW) {

      // still low after 5 seconds? reset wifi + check for more

      // reset wifi settings
      Serial.println("resetting wi-fi settings ...");

      saveCredentials("", "");

      // if it was released after 5 seconds, reset the chip

      Serial.println("restarting ...");
      ESP.restart();
      return;

    }

  }

  loadCredentials();

  if (!hasSSIDConfigured()) {
    Serial.println("Starting SoftAP ...");
    startSoftAp();
    //startWebServer();
  }
  connect = hasSSIDConfigured();

  Serial.println("Starting web server ...");
  startWebServer();
}

void loop () {

  if (connect) {
    Serial.println ( "Connect requested" );
    connect = false;
    connectWifi();
    lastConnectTry = millis();
  }

  if (hasSSIDConfigured()) {

    int s = WiFi.status();
    if (s == WL_IDLE_STATUS && millis() > (lastConnectTry + 60000) ) {
      /* If WLAN disconnected and idle try to connect */
      /* Don't set retry time too low as retry interfere the softAP operation */
      connect = true;
    }
    if (status != s) { // WLAN status change
      Serial.print ( "Status: " );
      Serial.println ( s );

/*
WL_NO_SHIELD = 255,
WL_IDLE_STATUS = 0,
WL_NO_SSID_AVAIL = 1
WL_SCAN_COMPLETED = 2
WL_CONNECTED = 3
WL_CONNECT_FAILED = 4
WL_CONNECTION_LOST = 5
WL_DISCONNECTED = 6
*/

      status = s;
      if (s == WL_CONNECTED) {
        /* Just connected to WLAN */
        Serial.println ( "" );
        Serial.print ( "Connected to " );
        Serial.println ( getSSID() );
        Serial.print ( "IP address: " );
        Serial.println ( WiFi.localIP() );

        Serial.println ("Starting web server.");
        startWebServer();

      } else if (s == WL_NO_SSID_AVAIL) {

        // if we cannot find the SSID, go to deep sleep.
        // perhaps the SSID is not available at the moment, but will become
        // available later. Prev code started the Soft AP, but we don't want
        // this, as there's no going out of this mode and drains the battery.
        deepsleep(60000);

        /*
        WiFi.disconnect();
        connect = false;
        startSoftAp();
        startWebServer();
        */
      }
    }

    if (s == WL_CONNECTED) {

      unsigned long now = millis();
      wifiHandleLoop();
      webServerHandleLoop();

    }

  } else {

    // Soft AP mode, handle
    wifiHandleLoop();
    webServerHandleLoop();
  }

}

// millis() rollover fix - http://arduino.stackexchange.com/questions/12587/how-can-i-handle-the-millis-rollover
void sleep(unsigned long ms) {            // ms: duration
  unsigned long start = millis();         // start: timestamp
  for (;;) {
    unsigned long now = millis();         // now: timestamp
    unsigned long elapsed = now - start;  // elapsed: duration
    if (elapsed >= ms)                    // comparing durations: OK
      return;
  }
}

void deepsleep(unsigned long micros) {
  Serial.println("Going into deep sleep");
  delay(100);
  ESP.deepSleep(micros);
}
